# haskell-abap-quasiquotes README

Prototype for a VSCode Extension for ABAP Syntax Highlighting inside Haskell QuasiQuotes

# Installation

Download .vsix file from [the Downloads section of this repository](https://bitbucket.org/jessika_rockel/haskell-abap-quasiquotes/downloads),

then run `$ code --install-extension haskell-abap-quasiquotes-0.1.0.vsix`